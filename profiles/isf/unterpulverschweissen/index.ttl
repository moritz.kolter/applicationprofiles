@base <https://purl.org/coscine/ap/isf/submergedArcWelding/> .

@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

@prefix dcterms: <http://purl.org/dc/terms/> .

@prefix isf: <https://purl.org/coscine/terms/isf#> .
@prefix coscineISFsubmergedArcWelding: <https://purl.org/coscine/ap/isf/submergedArcWelding#> .

<https://purl.org/coscine/ap/isf/submergedArcWelding/>
  dcterms:license <http://spdx.org/licenses/MIT> ;
  dcterms:publisher <https://isf.rwth-aachen.de/> ;
  dcterms:title  "ISF submerged arc welding"@en, "ISF Unterpulverschweißen"@de ;

  a sh:NodeShape ;
  sh:targetClass <https://purl.org/coscine/ap/isf/submergedArcWelding/> ;
  sh:closed true ;

  sh:property [
    sh:path rdf:type ;
  ] ;

  sh:property coscineISFsubmergedArcWelding:voltage ;
  sh:property coscineISFsubmergedArcWelding:current ;
  sh:property coscineISFsubmergedArcWelding:wireFeedSpeed ;
  sh:property coscineISFsubmergedArcWelding:weldingSpeed ;
  sh:property coscineISFsubmergedArcWelding:powerContactTubeDistance ;
  sh:property coscineISFsubmergedArcWelding:weldingFillerMaterialDiameter ;
  sh:property coscineISFsubmergedArcWelding:weldingFillerMaterial ;
  sh:property coscineISFsubmergedArcWelding:flux ;
  sh:property coscineISFsubmergedArcWelding:typeOfCurrent ;
  sh:property coscineISFsubmergedArcWelding:polarity ;
  sh:property coscineISFsubmergedArcWelding:balance ;
  sh:property coscineISFsubmergedArcWelding:torchOffset ;
  sh:property coscineISFsubmergedArcWelding:frequency ;
  sh:property coscineISFsubmergedArcWelding:workpieceMaterial ;
  sh:property coscineISFsubmergedArcWelding:jointPreparation ;
  sh:property coscineISFsubmergedArcWelding:angleOfBevel ;
  sh:property coscineISFsubmergedArcWelding:gap ;
  sh:property coscineISFsubmergedArcWelding:workpieceDimesion ;
  sh:property coscineISFsubmergedArcWelding:fillerMetalInclinationAngle .

coscineISFsubmergedArcWelding:voltage
  sh:path isf:voltage ;
  sh:order 0 ;
  sh:minCount 1 ;
  sh:maxCount 2 ;
  sh:datatype xsd:string ;
  sh:name "Voltage"@en, "Spannung"@de .

coscineISFsubmergedArcWelding:current
  sh:path isf:current ;
  sh:order 1 ;
  sh:minCount 1 ;
  sh:maxCount 2 ;
  sh:datatype xsd:string ;
  sh:name "Current"@en, "Stromstärke"@de .

coscineISFsubmergedArcWelding:wireFeedSpeed
  sh:path isf:wireFeedSpeed ;
  sh:order 2 ;
  sh:minCount 1 ;
  sh:maxCount 2 ;
  sh:datatype xsd:string ;
  sh:name "Wire feed speed "@en, "Drahtvorschubgeschwindigkeit"@de .

coscineISFsubmergedArcWelding:weldingSpeed
  sh:path isf:weldingSpeed ;
  sh:order 3 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Welding speed"@en, "Schweißgeschwindigkeit"@de .

coscineISFsubmergedArcWelding:powerContactTubeDistance
  sh:path isf:powerContactTubeDistance ;
  sh:order 4 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Power contact tube distance"@en, "Stromkontaktrohrabstand"@de .

coscineISFsubmergedArcWelding:weldingFillerMaterialDiameter
  sh:path isf:weldingFillerMaterialDiameter ;
  sh:order 5 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Wire diameter"@en, "Drahtdurchmesser"@de .

coscineISFsubmergedArcWelding:weldingFillerMaterial
  sh:path isf:weldingFillerMaterial ;
  sh:order 6 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Welding filler material"@en, "Schweißzusatz"@de .

coscineISFsubmergedArcWelding:flux
  sh:path isf:flux ;
  sh:order 7 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Flux"@en, "Schweißpulver"@de .

coscineISFsubmergedArcWelding:typeOfCurrent
  sh:path isf:typeOfCurrent ;
  sh:order 8 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Type of current"@en, "Stromart"@de .

coscineISFsubmergedArcWelding:polarity
  sh:path isf:polarity ;
  sh:order 9 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Polarity"@en, "Polung"@de .

coscineISFsubmergedArcWelding:balance
  sh:path isf:balance ;
  sh:order 10 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "balance"@en, "Balance"@de .

coscineISFsubmergedArcWelding:torchOffset
  sh:path isf:torchOffset ;
  sh:order 11 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Torch Offset"@en, "Brenner Versatz"@de .

coscineISFsubmergedArcWelding:frequency
  sh:path isf:frequency ;
  sh:order 12 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "frequency"@en, "Frequenz"@de .

coscineISFsubmergedArcWelding:workpieceMaterial
  sh:path isf:workpieceMaterial ;
  sh:order 13 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Base Material"@en, "Grundwerkstoff"@de .

coscineISFsubmergedArcWelding:jointPreparation
  sh:path isf:jointPreparation ;
  sh:order 14 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "JointPreparation"@en, "Schweißnahtvorbereitung"@de .

coscineISFsubmergedArcWelding:angleOfBevel
  sh:path isf:angleOfBevel ;
  sh:order 15 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Angle of bevel"@en, "Flankenwinkel"@de .

coscineISFsubmergedArcWelding:gap
  sh:path isf:gap ;
  sh:order 16 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "gap"@en, "Spalt"@de .

coscineISFsubmergedArcWelding:workpieceDimesion
  sh:path isf:workpieceDimesion ;
  sh:order 17 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Plate thickness"@en, "Blechdicke"@de .

coscineISFsubmergedArcWelding:fillerMetalInclinationAngle
  sh:path isf:fillerMetalInclinationAngle ;
  sh:order 18 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Wire inclination"@en, "Anstellung Draht"@de .
